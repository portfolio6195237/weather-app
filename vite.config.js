import { defineConfig } from 'vite';
import eslint from 'vite-plugin-eslint';
import { fileURLToPath, URL } from 'url';

export default defineConfig({
  publicDir: 'public',
  root: './',
  build: {
    outDir: 'dist',
  },
  plugins: [
    eslint({
      cache: false,
      fix: true,
    }),
  ],
  resolve: {
    alias: [
      {
        find: '@icons',
        replacement: fileURLToPath(
          new URL('./src/assets/images/icons', import.meta.url),
        ),
      },
      {
        find: '@images',
        replacement: fileURLToPath(
          new URL('./src/assets/images', import.meta.url),
        ),
      },
      {
        find: '@bg',
        replacement: fileURLToPath(
          new URL('./src/assets/images/bg', import.meta.url),
        ),
      },
    ],
  },
});
