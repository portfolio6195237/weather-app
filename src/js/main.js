import 'the-new-css-reset/css/reset.css';
import '../styles/style.css';

import mainInfo from './templates/mainInfo';
import navBar from './templates/navBar';
import activities from './templates/activities';
import hourlyForecast from './templates/hourlyForecast';
import airConditions from './templates/airConditions';

import ActivitiesWidget from './widgets/ActivitiesWidget';
import MainInfoWidget from './widgets/MainInfoWidget';
import HourlyForecastWidget from './widgets/HourlyForecastWidget';
import AirConditionsWidget from './widgets/AirConditionsWidget';

import collectUserData from './utils/APIs/collectUserData';
import updateApp from './utils/updateApp';

document.querySelector('.app').innerHTML = `
  ${mainInfo}
  <div class="main">
    <div class="left-column">
      ${navBar}
    </div>
    <div class="middle-column">
      ${activities}
      ${hourlyForecast}
    </div>
    <div class="right-column">
      ${airConditions}
    </div>
  </div>
`;

const activitiesWidget = new ActivitiesWidget({
  containerID: 'activities',
});
const mainInfoWidget = new MainInfoWidget({ containerID: 'main-info' });
const hourlyForecastWidget = new HourlyForecastWidget({
  chartID: 'chart',
});
const airConditionsWidget = new AirConditionsWidget({
  containerID: 'air-conditions',
});

airConditionsWidget.initButtonsHandlers();
mainInfoWidget.initFormHandler({
  activitiesWidget,
  mainInfoWidget,
  hourlyForecastWidget,
  airConditionsWidget,
});
mainInfoWidget.initInputHandler();

collectUserData().then((cityData) => {
  updateApp({
    activitiesWidget,
    mainInfoWidget,
    hourlyForecastWidget,
    airConditionsWidget,
    cityData,
  });
});
