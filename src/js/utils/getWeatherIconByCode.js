export default async function getWeatherIconByCode(code) {
  // динамический импорт для краткости кода
  const { default: weatherIcon } = await import(
    `../../assets/images/icons/weather/${code}.svg`
  );

  return weatherIcon;
}
