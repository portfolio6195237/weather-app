// проверка существования свойств любой вложенности в объекте obj
export default function checkObjProperty(obj, propPath, objName) {
  const props = propPath.split('.');
  let currentObj = obj;

  props.forEach((prop) => {
    if (currentObj[prop] === undefined) {
      throw new Error(
        `Свойство '${prop}' не существует в объекте '${objName}'.`,
      );
    }
    currentObj = currentObj[prop];
  });
}
