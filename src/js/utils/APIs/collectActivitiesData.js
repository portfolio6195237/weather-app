import parseActivitiesData from '../parsers/parseActivitiesData';

export default async function collectActivitiesData({ queryParameters }) {
  const baseURL = import.meta.env.VITE_GOOGLE_PLACES_BASE_URL;
  const proxyURL = import.meta.env.VITE_PROXY_URL;
  const APIKey = import.meta.env.VITE_GOOGLE_API_KEY;
  const url = `${proxyURL}${baseURL}/nearbysearch/json?${new URLSearchParams({
    ...{
      radius: '10000',
      type: 'restaurant',
      key: APIKey,
    },
    ...queryParameters,
  })}`;

  return fetch(url)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }

      throw new Error(`Error ${response.cod}: ${response.me}`);
    })
    .then(async (data) => {
      const parsedData = parseActivitiesData(data);

      return parsedData;
    });
}
