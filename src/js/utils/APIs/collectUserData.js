import parseUserData from '../parsers/parseUserData';

export default async function collectUserData() {
  const baseURL = import.meta.env.VITE_COORDS_BY_IP_ENDPOINT;

  return fetch(baseURL)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }

      throw new Error(`Error ${response.cod}: ${response.me}`);
    })
    .then(async (data) => {
      const parsedData = parseUserData(data);

      return parsedData;
    });
}
