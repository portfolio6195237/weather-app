import parseCityData from '../parsers/parseCityData';

export default async function collectCityData(city) {
  const baseURL = import.meta.env.VITE_COORDS_BY_CITY_ENDPOINT;
  const APIKey = import.meta.env.VITE_GOOGLE_API_KEY;
  const url = `${baseURL}?${new URLSearchParams({
    address: city,
    key: APIKey,
  })}`;

  return fetch(url)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }

      throw new Error(`Error ${response.cod}: ${response.me}`);
    })
    .then(async ({ results }) => {
      if (results.length) {
        const parsedData = parseCityData(results[0]);

        return parsedData;
      }

      throw new Error('City not found!');
    });
}
