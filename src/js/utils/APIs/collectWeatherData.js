import parseMainForecastData from '../parsers/parseMainForecastData';
import parseHistoricalForecastData from '../parsers/parseHistoricalForecastData';

export default async function collectWeatherData({ queryParameters }) {
  const baseURL = import.meta.env.VITE_FORECAST_BASE_URL;
  const APIKey = import.meta.env.VITE_FORECAST_API_KEY;
  const url = `${baseURL}?${new URLSearchParams({
    ...{
      exclude: 'minutely',
      units: 'metric',
      appid: APIKey,
    },
    ...queryParameters,
  })}`;

  return fetch(url)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }

      throw new Error(`Error ${response.cod}: ${response.me}`);
    })
    .then(async (data) => {
      const parsedData = parseMainForecastData(data);

      if (parsedData.daily !== null) {
        try {
          const promises = [];
          const secondsPerDay = 86400;
          const minDailyDataIndex = parsedData.daily.findIndex(Boolean);
          const minTimestamp = parsedData.daily[minDailyDataIndex].timestamp;

          for (let i = 1; i <= minDailyDataIndex; i += 1) {
            const dayBeforeTimestamp = minTimestamp - secondsPerDay * i;

            const timemachineURL = `${baseURL}/timemachine?${new URLSearchParams({ ...queryParameters, ...{ appid: APIKey, dt: dayBeforeTimestamp, units: 'metric' } })}`;

            promises.push(
              fetch(timemachineURL).then((response) => response.json()),
            );
          }

          const responses = await Promise.all(promises);

          responses.reverse().forEach((response, index) => {
            parsedData.daily[index] = parseHistoricalForecastData(response);
          });
        } catch (e) {
          console.error(e);
          parsedData.daily = null;
        }
      }

      return parsedData;
    });
}
