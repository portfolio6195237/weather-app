export default function setGlobalSettingsByCode(code) {
  if (!code) {
    return;
  }

  const isNighttime = code.slice(-1) === 'n';

  if (isNighttime) {
    document.body.classList.add('dark');
  } else {
    document.body.classList.remove('dark');
  }

  document.body.style.backgroundImage = `var(--bg-${code})`;
}
