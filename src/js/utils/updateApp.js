import collectActivitiesData from './APIs/collectActivitiesData';
import collectWeatherData from './APIs/collectWeatherData';
import setGlobalSettingsByCode from './setGlobalSettingsByCode';

export default function updateApp({
  activitiesWidget,
  mainInfoWidget,
  hourlyForecastWidget,
  airConditionsWidget,
  cityData,
}) {
  const { cityName, lat, lon } = cityData;

  collectActivitiesData({
    queryParameters: {
      location: `${lat},${lon}`,
    },
  }).then((data) => {
    activitiesWidget.render({ data, baseGeoPoint: { lat, lon } });
  });

  collectWeatherData({
    queryParameters: {
      lat,
      lon,
    },
  })
    .then(({ current, hourly, daily }) => {
      setGlobalSettingsByCode(current.weatherCode);
      mainInfoWidget.render({
        cityName,
        data: current,
      });
      hourlyForecastWidget.render(hourly);
      airConditionsWidget.render(daily);
    })
    .catch((error) => {
      if (!window.navigator.onLine) {
        window.location.href = '/';
      }

      console.error(error);
    });
}
