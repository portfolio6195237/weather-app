import timestampToDate from '../converters/timestampToDate';
import timestampToTime from '../converters/timestampToTime';
import timestampToDay from '../converters/timestampToDay';
import milesToKmSpeed from '../converters/milesToKmSpeed';
import checkObjProperty from '../checkObjProperty';

export default function parseMainForecastData({
  timezone_offset: timezoneOffset,
  current = {},
  daily = [],
  hourly = [],
}) {
  let currentForecast;

  try {
    const objName = 'current';
    ['dt', 'weather.0.main', 'weather.0.icon', 'temp'].forEach((prop) => {
      checkObjProperty(current, prop, objName);
    });

    currentForecast = {
      dateLabel: timestampToDate(current.dt),
      weatherName: current.weather[0].main,
      temp: Math.round(current.temp),
      weatherCode: current.weather[0].icon,
    };
  } catch (e) {
    console.error(e);
    currentForecast = null;
  }

  let dailyForecast = Array(3).fill(null);

  try {
    const objName = 'daily';
    const currentDayIndex = 0;
    const lastDayIndex = 3;
    for (let i = currentDayIndex; i <= lastDayIndex; i += 1) {
      const dailyData = daily[i];

      [
        'dt',
        'pop',
        'feels_like',
        'wind_speed',
        'uvi',
        'weather.0.icon',
      ].forEach((prop) => {
        checkObjProperty(dailyData, prop, objName);
      });

      const {
        dt,
        pop,
        feels_like: feelsLikeTemp,
        wind_speed: wind,
        uvi,
        weather,
      } = dailyData;

      const dateTimestamp = i === 0 ? current.dt || Date.now() / 1000 : dt;

      dailyForecast.push({
        timestamp: dateTimestamp,
        GMTLabel: timestampToTime(dateTimestamp),
        day: timestampToDay(dateTimestamp),
        weatherCode: weather[0].icon,
        feelsLikeTemp: Math.round(feelsLikeTemp.day),
        wind: milesToKmSpeed(wind),
        chanceOfRain: pop.toFixed(2) * 100,
        UVI: Math.round(uvi),
      });
    }
  } catch (e) {
    console.error(e);
    dailyForecast = null;
  }

  const isMobileScreen = window.screen.width < 481;
  let hourlyForecast;

  try {
    const objName = 'hourly';
    ['0.temp'].forEach((prop) => {
      checkObjProperty(daily, prop, objName);
    });

    hourlyForecast = [
      { x: 0, y: hourly[0].temp },
      {
        markerSize: 5,
        x: 90,
        y: currentForecast.temp,
        wind: milesToKmSpeed(current.wind_speed),
        time: 'Now',
        weatherCode: currentForecast.weatherCode,
      },
    ];

    const xAxisPointsGap = 90;
    let maxHourlyForecastLength = 9;
    if (isMobileScreen) {
      maxHourlyForecastLength = 6;
    }
    for (let i = 2; i < maxHourlyForecastLength; i += 1) {
      const hourlyData = hourly[i - 1];

      ['temp', 'wind_speed', 'dt', 'weather'].forEach((prop) => {
        checkObjProperty(hourlyData, prop, objName);
      });

      const { temp, wind_speed: wind, dt, weather } = hourlyData;

      hourlyForecast.push({
        x: hourlyForecast[i - 1].x + xAxisPointsGap,
        y: Math.round(temp),
        wind: milesToKmSpeed(wind),
        time: timestampToTime(dt, timezoneOffset),
        weatherCode: weather[0].icon,
      });
    }
  } catch (e) {
    console.error(e);
    hourlyForecast = null;
  }

  return {
    current: currentForecast,
    daily: dailyForecast,
    hourly: hourlyForecast,
  };
}
