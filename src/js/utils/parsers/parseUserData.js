export default function parseUserData({
  cityName = 'New York',
  latitude = '40.73',
  longitude = '-73.93',
}) {
  return {
    cityName,
    lat: latitude.toFixed(2),
    lon: longitude.toFixed(2),
  };
}
