import checkObjProperty from '../checkObjProperty';

export default function parseCityData(data) {
  const objName = 'city data';
  ['address_components.0.long_name', 'geometry.location'].forEach((prop) => {
    checkObjProperty(data, prop, objName);
  });

  const cityName = data.address_components[0].long_name;
  const { lat, lng: lon } = data.geometry.location;

  return {
    cityName,
    lat: lat.toFixed(2),
    lon: lon.toFixed(2),
  };
}
