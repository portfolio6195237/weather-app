import timestampToTime from '../converters/timestampToTime';
import timestampToDay from '../converters/timestampToDay';
import milesToKmSpeed from '../converters/milesToKmSpeed';
import checkObjProperty from '../checkObjProperty';

export default function parseHistoricalForecastData(data) {
  const objName = 'historical data';
  ['data.0'].forEach((prop) => {
    checkObjProperty(data, prop, objName);
  });

  const historicalDayData = data.data[0];
  ['dt', 'feels_like', 'wind_speed', 'uvi', 'weather.0.icon'].forEach(
    (prop) => {
      checkObjProperty(historicalDayData, prop, objName);
    },
  );

  const {
    dt,
    feels_like: feelsLikeTemp,
    wind_speed: wind,
    uvi,
    weather,
  } = historicalDayData;

  return {
    timestamp: dt,
    GMTLabel: timestampToTime(dt),
    day: timestampToDay(dt),
    weatherCode: weather[0].icon,
    feelsLikeTemp: Math.round(feelsLikeTemp),
    wind: milesToKmSpeed(wind),
    chanceOfRain: null,
    UVI: Math.round(uvi),
  };
}
