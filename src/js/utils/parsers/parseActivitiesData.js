export default function parseActivitiesData({ results = [] }) {
  const maxActivitiesCount = 4;
  const selectedActivities = [];
  let passedActivitiesCounter = 0;

  while (
    selectedActivities.length < maxActivitiesCount &&
    passedActivitiesCounter < results.length
  ) {
    const currentIndex = passedActivitiesCounter;

    if (results[currentIndex].photos) {
      const { name, photos, geometry } = results[currentIndex];
      const {
        photo_reference: photoReference,
        html_attributions: [anchorTag],
      } = photos[0];
      const { location: coords } = geometry;

      selectedActivities.push({
        name,
        photoReference,
        link: anchorTag.match(/href="([^"]+)"/)[1],
        coords: { lat: coords.lat, lon: coords.lng },
      });
    }

    passedActivitiesCounter += 1;
  }

  return selectedActivities;
}
