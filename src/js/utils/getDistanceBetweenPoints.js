import degreesToRadians from './converters/degreesToRadians';

export default function getDistanceBetweenPoints({ firstPoint, secondPoint }) {
  // Радиус Земли в километрах
  const R = 6371.0;

  // Преобразование координат из градусов в радианы
  const radLat1 = degreesToRadians(firstPoint.lat);
  const radLon1 = degreesToRadians(firstPoint.lon);
  const radLat2 = degreesToRadians(secondPoint.lat);
  const radLon2 = degreesToRadians(secondPoint.lon);

  // Разница широты и долготы
  const dLat = radLat2 - radLat1;
  const dLon = radLon2 - radLon1;

  // Формула Haversine
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(radLat1) *
      Math.cos(radLat2) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  // Расстояние в километрах
  const distance = (R * c).toFixed(1).replace(/\.0$/, '');

  return `${distance} km away`;
}
