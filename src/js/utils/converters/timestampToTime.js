export default function timestampToTime(timestamp, offset) {
  let date = new Date(timestamp * 1000);

  if (offset) {
    date = new Date(date.getTime() + offset * 1000);
  }

  const timeFormatter = new Intl.DateTimeFormat('en-US', {
    hour: 'numeric',
    minute: '2-digit',
    hour12: false,
  });

  const formattedTime = timeFormatter.format(date).replace(/^0/, '');

  return formattedTime;
}
