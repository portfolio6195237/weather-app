export default function convertMilesSpeedToKm(milesPerHour) {
  // коэффициент перевода миль в час в километры в час
  const conversionFactor = 1.60934;

  const kilometersPerHour = milesPerHour * conversionFactor;

  // округляем, удаляя при необходимости 0 с точкой
  return kilometersPerHour.toFixed(1).replace(/\.0$/, '');
}
