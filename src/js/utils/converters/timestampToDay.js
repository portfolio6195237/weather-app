// конвертирование UTC метки в секундах в формат дня вида "SUN"
export default function timestampToDay(timestamp) {
  const date = new Date(timestamp * 1000);

  const dateFormatter = new Intl.DateTimeFormat('en', { weekday: 'short' });

  const dayOfWeek = dateFormatter.format(date);

  return dayOfWeek.toUpperCase();
}
