// конвертирование UTC метки в секундах в формат "Sunday | 12 Dec 2023"
export default function timestampToDate(timestamp) {
  const date = new Date(timestamp * 1000);

  const dateFormatter = new Intl.DateTimeFormat('en-US', {
    weekday: 'long',
    day: '2-digit',
    month: 'short',
    year: 'numeric',
  });

  const formattedDate = dateFormatter
    .format(date)
    .replace(/(\w+), (\w+) 0?(\d+), (\d+)/, '$1 | $3 $2 $4');

  return formattedDate;
}
