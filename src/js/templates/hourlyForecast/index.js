import clock from '@icons/clock.svg';
import './style.css';

const hourlyForecast = `
  <div class="chart-container container">
    <h4 class="widget-title forecast-title">
      <img src="${clock}">&nbsp;24-hour forecast
    </h4>
    <div class="chart" id="chart"></div>
  </div>
`;

export default hourlyForecast;
