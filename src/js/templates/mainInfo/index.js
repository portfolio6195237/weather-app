import city from '@icons/nav/cities.svg';
import arrowRight from '@icons/arrows/right.svg';
import './style.css';

const mainInfo = `
  <header class="main-info" id="main-info">
    <div class="left-column">
      <form class="city-form">
        <img class="city-form-icon" src="${city}">
        <input class="city-form-input" type="text" value="New York">
        <button class="city-form-button" type="submit">
          <img src="${arrowRight}">
        </button>
      </form>
      <div class="notification">
        City not found! Please, try again.
      </div>
      <div class="current-weather">
        <div class="current-weather-name">
        </div>
        <div class="current-weather-temperature">
        </div>
        <div class="current-date">
        </div>
      </div>
    </div>
    <div class="right-column">
      <img class="current-weather-image">
    </div>
  </header>
`;

export default mainInfo;
