import weather from '@icons/nav/weather.svg';
import explore from '@icons/nav/explore.svg';
import cities from '@icons/nav/cities.svg';
import settings from '@icons/nav/settings.svg';
import avatarSlug from '@images/avatar-slug.jpg';
import './style.css';

const navBar = `
  <nav class="nav-bar-container container">
    <div class="avatar">
      <img src="${avatarSlug}" alt="User avatar">
    </div>
    <div class="nav-bar">
      <div class="nav-bar-item weather">
        <div class="nav-bar-item-icon">
          <img src="${weather}">
        </div>
        <div class="nav-bar-item-description">
          weather
        </div>
      </div>
      <div class="nav-bar-item explore">
        <div class="nav-bar-item-icon">
          <img src="${explore}">
        </div>
        <div class="nav-bar-item-description">
          explore
        </div>
      </div>
      <div class="nav-bar-item cities">
        <div class="nav-bar-item-icon">
          <img src="${cities}">
        </div>
        <div class="nav-bar-item-description">
          cities
        </div>
      </div>
      <div class="nav-bar-item settings">
        <div class="nav-bar-item-icon">
          <img src="${settings}">
        </div>
        <div class="nav-bar-item-description">
          settings
        </div>
      </div>
    </div>
  </nav>
`;

export default navBar;
