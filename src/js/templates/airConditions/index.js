import clock from '@icons/clock.svg';
import temperature from '@icons/air/real-feel.svg';
import wind from '@icons/air/wind.svg';
import rain from '@icons/air/chance-of-rain.svg';
import uv from '@icons/air/uv-index.svg';
import arrow from '@icons/arrows/right.svg';
import './style.css';

const airConditions = `
  <div class="air-conditions-container container" id="air-conditions">
    <div class="daily-forecast">
      <button class="daily-forecast-button-left">
        <img src="${arrow}">
      </button>
      <div class="daily-forecast-data"></div>
      <button class="daily-forecast-button-right">
        <img src="${arrow}">
      </button>
    </div>
    <div class="air-conditions-time">
      <img class="air-conditions-time-img" src="${clock}">
      <span class="air-conditions-time-label">
        Requested data unavailable.
      </span>
    </div>
    <div class="air-conditions">
      <div class="air-conditions-title">AIR CONDITIONS</div>
      <div class="air-conditions-item temperature">
        <div class="air-conditions-icon">
          <img src="${temperature}">
        </div>
        <div class="air-conditions-info">
          <div class="air-conditions-label">
            Real Feel
          </div>
          <div class="air-conditions-value">
            -
          </div>
        </div>
      </div>
      <div class="air-conditions-item wind">
        <div class="air-conditions-icon">
          <img src="${wind}">
        </div>
        <div class="air-conditions-info">
          <div class="air-conditions-label">
            Wind
          </div>
          <div class="air-conditions-value">
            -
          </div>
        </div>
      </div>
      <div class="air-conditions-item rain">
        <div class="air-conditions-icon">
          <img src="${rain}">
        </div>
        <div class="air-conditions-info">
          <div class="air-conditions-label">
            Chance of rain
          </div>
          <div class="air-conditions-value">
            -
          </div>
        </div>
      </div>
      <div class="air-conditions-item uv">
        <div class="air-conditions-icon">
          <img src="${uv}">
        </div>
        <div class="air-conditions-info">
          <div class="air-conditions-label">
            UV index
          </div>
          <div class="air-conditions-value">
            -
          </div>
        </div>
      </div>
    </div>
  </div>
`;

export default airConditions;
