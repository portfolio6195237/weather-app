import heart from '@icons/heart.svg';
import './style.css';

const activities = `
  <div class="activities-container container">
    <h4 class="widget-title activities-title">
      <img src="${heart}">&nbsp;Activities in your area
    </h4>
    <div class="activities" id="activities"></div>
  </div>
`;

export default activities;
