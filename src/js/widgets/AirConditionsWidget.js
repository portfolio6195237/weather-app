import getWeatherIconByCode from '../utils/getWeatherIconByCode';

export default class AirConditionsWidget {
  constructor({ containerID }) {
    this.mainContainer = document.body.querySelector(`#${containerID}`);

    // основные элементы ленты ежедневного прогноза
    this.dailyForecastData = this.mainContainer.querySelector(
      '.daily-forecast-data',
    );
    this.dailyForecastButtonLeft = this.mainContainer.querySelector(
      '.daily-forecast-button-left',
    );
    this.dailyForecastButtonRight = this.mainContainer.querySelector(
      '.daily-forecast-button-right',
    );

    // элементы, содержащие информацию о состоянии воздуха дня
    this.timeLabel = this.mainContainer.querySelector(
      '.air-conditions-time-label',
    );
    this.realFeelTemp = this.mainContainer.querySelector(
      '.temperature .air-conditions-value',
    );
    this.wind = this.mainContainer.querySelector('.wind .air-conditions-value');
    this.rain = this.mainContainer.querySelector('.rain .air-conditions-value');
    this.UVI = this.mainContainer.querySelector('.uv .air-conditions-value');

    // флаг для задержки между кликами в 1 секунду
    this.buttonIsClicked = false;
  }

  disableButtonClickForASecond() {
    this.buttonIsClicked = true;

    setTimeout(() => {
      this.buttonIsClicked = false;
    }, 1000);
  }

  initButtonsHandlers() {
    const offsetStep = 62;

    // Обработчик клика для кнопки влево
    this.dailyForecastButtonLeft.addEventListener('click', () => {
      // Ранний выход, если кнопка отключена
      if (
        this.dailyForecastButtonLeft.classList.contains('disabled') ||
        this.buttonIsClicked
      ) {
        return;
      }
      this.disableButtonClickForASecond();

      // Сдвиг ленты влево
      const currentLeft =
        parseInt(window.getComputedStyle(this.dailyForecastData).left, 10) || 0;
      this.dailyForecastData.style.left = `${currentLeft - offsetStep}px`;

      // Увеличение индекса для корректного отображения среднего элемента
      this.currentDayIndex += 1;

      // Обновление всех данных и классов
      this.renderDayAirConditions(this.data[this.currentDayIndex]);
      this.updateDaysClasses();

      // Отключение/включение кнопок в крайних положениях
      if (this.currentDayIndex === this.dailyForecastData.children.length - 1) {
        this.dailyForecastButtonLeft.classList.add('disabled');
      }

      this.dailyForecastButtonRight.classList.remove('disabled');
    });

    // Обработчик клика для кнопки вправо
    this.dailyForecastButtonRight.addEventListener('click', () => {
      // Ранний выход, если кнопка отключена
      if (
        this.dailyForecastButtonRight.classList.contains('disabled') ||
        this.buttonIsClicked
      ) {
        return;
      }
      this.disableButtonClickForASecond();

      // Сдвиг ленты вправо
      const currentLeft =
        parseInt(window.getComputedStyle(this.dailyForecastData).left, 10) || 0;
      this.dailyForecastData.style.left = `${currentLeft + offsetStep}px`;

      // Уменьшение индекса для корректного отображения среднего элемента
      this.currentDayIndex -= 1;

      // Обновление всех данных и классов
      this.renderDayAirConditions(this.data[this.currentDayIndex]);
      this.updateDaysClasses();

      // Отключение/включение кнопок в крайних положениях
      if (this.currentDayIndex === 0) {
        this.dailyForecastButtonRight.classList.add('disabled');
      }

      this.dailyForecastButtonLeft.classList.remove('disabled');
    });
  }

  getDayClassesByIndex(index) {
    if (index === this.currentDayIndex) {
      return 'day-info middle';
    }

    if (Math.abs(index - this.currentDayIndex) === 1) {
      return 'day-info near-middle';
    }

    return 'day-info from-edge';
  }

  updateDaysClasses() {
    const { children } = this.dailyForecastData;

    for (let i = 0; i < children.length; i += 1) {
      const dayInfo = children[i];

      dayInfo.setAttribute('class', this.getDayClassesByIndex(i));
    }
  }

  renderDayAirConditions(data) {
    const { GMTLabel, feelsLikeTemp, wind, chanceOfRain, UVI } = data;

    this.timeLabel.innerText = GMTLabel;
    this.realFeelTemp.innerText = `${feelsLikeTemp}°`;
    this.wind.innerText = `${wind} km/hr`;
    this.rain.innerText = chanceOfRain === null ? '-' : `${chanceOfRain}%`;
    this.UVI.innerText = UVI;
  }

  renderDailyForecast() {
    this.data.forEach(({ day, weatherCode }, index) => {
      // Создаем контейнер для информации дня
      const dayInfo = document.createElement('div');

      // Добавляем текстовый элемент с названием дня
      const dayTextElement = document.createElement('div');
      dayTextElement.setAttribute('class', 'day-text');
      dayTextElement.textContent = day; // Используем переменную day для названия дня
      dayInfo.appendChild(dayTextElement); // Добавляем элемент с названием дня в контейнер

      // Создаем изображение
      const imageElement = document.createElement('img');
      imageElement.setAttribute('class', 'day-icon');
      getWeatherIconByCode(weatherCode).then((image) => {
        imageElement.setAttribute('src', image);
      });

      // Вставляем изображение внутрь контейнера
      dayInfo.appendChild(imageElement);

      // Определяем класс для контейнера в зависимости от его позиции относительно this.currentDayIndex
      dayInfo.setAttribute('class', this.getDayClassesByIndex(index));

      // Вставляем новый элемент в общий контейнер
      this.dailyForecastData.appendChild(dayInfo);
    });
  }

  render(data) {
    this.dailyForecastData.innerHTML = '';
    this.mainContainer.classList.remove('unavailable');

    if (!data) {
      this.mainContainer.classList.add('unavailable');

      this.timeLabel.innerText = 'Requested data unavailable.';
      this.realFeelTemp.innerText = '-';
      this.wind.innerText = '-';
      this.rain.innerText = '-';
      this.UVI.innerText = '-';

      return;
    }

    this.data = data;
    this.currentDayIndex = Math.floor(data.length / 2);

    this.renderDailyForecast();
    this.renderDayAirConditions(data[this.currentDayIndex]);
  }
}
