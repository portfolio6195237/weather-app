import * as CanvasJS from '@canvasjs/charts';
import getWeatherIconByCode from '../utils/getWeatherIconByCode';

export default class HourlyForecastWidget {
  constructor({ chartID }) {
    this.chartID = chartID; // id графика в html шаблоне

    this.chartContainer = document.body.querySelector(`#${chartID}`);

    this.pointInfoWidth = 70; // ширина поля с информацией для точки
    this.pointInfoTop = 30; // отступ от точки до поля с информацией
  }

  // функция позиционирования информации для точки
  positionInfo(info, index) {
    const infoCenter = this.chart.axisX[0].convertValueToPixel(
      this.chart.data[0].dataPoints[index].x,
    );
    const infoTop = this.chart.axisY[0].convertValueToPixel(
      this.chart.data[0].dataPoints[index].y,
    );

    const style = {
      width: `${this.pointInfoWidth}px`,
      left: `${infoCenter - this.pointInfoWidth / 2}px`,
      top: `${infoTop + this.pointInfoTop}px`,
    };

    Object.assign(info.style, style);
  }

  // функция добавления информации для точек
  addPointsInfo() {
    const loopLength = this.chart.data[0].dataPoints.length - 1;

    for (let i = 1; i < loopLength; i += 1) {
      const { time, wind, weatherCode } = this.chart.data[0].dataPoints[i];

      // создание контейнера для информации
      const info = document.createElement('div');

      if (time === 'Now') {
        info.setAttribute('class', 'first-point-info point-info');
        info.style.setProperty('--data-top', `${-this.pointInfoTop + 5}px`);
      } else {
        info.setAttribute('class', 'point-info');
      }

      // добавление иконки погоды в контейнер
      const img = document.createElement('img');
      img.setAttribute('class', 'point-info-img');
      getWeatherIconByCode(weatherCode).then((data) => {
        img.setAttribute('src', data);
      });

      info.appendChild(img);

      // добавление информации о ветре и текущем времени в контейнер
      const windBlock = document.createElement('span');
      windBlock.innerText = `${wind} km/h`;
      const timeBlock = document.createElement('span');
      timeBlock.innerText = time;

      info.appendChild(windBlock);
      info.appendChild(timeBlock);

      // помещение контейнера с информацией на график и позиционирование
      const canvasContainer = this.chartContainer.querySelector(
        '.canvasjs-chart-container',
      );
      canvasContainer.appendChild(info);

      this.positionInfo(info, i);
    }
  }

  // функция инициализации графика
  render(data) {
    this.chartContainer.innerHTML = '';
    this.chartContainer.classList.remove('unavailable');

    if (!data) {
      this.chartContainer.classList.add('unavailable');

      return;
    }

    // определяем фоновый цвет и семейство шрифтов графика по родителю
    const parentContainer = this.chartContainer.closest('.container');
    const computedStyles = window.getComputedStyle(parentContainer);
    const backgroundColor = computedStyles.getPropertyValue('background-color');
    const fontFamily = computedStyles.getPropertyValue('font-family');

    // находим крайние точки для расположения графика в видимой области
    const { maxY, minY } = data.reduce(
      (acc, point) => {
        acc.maxY = Math.max(acc.maxY, point.y);
        acc.minY = Math.min(acc.minY, point.y);
        return acc;
      },
      { maxY: -Infinity, minY: Infinity },
    );

    this.chart = new CanvasJS.Chart(this.chartID, {
      backgroundColor,
      toolTip: {
        enabled: false,
      },
      axisX: {
        tickThickness: 0,
        lineThickness: 0,
        labelFormatter: () => '',
      },
      axisY: {
        gridThickness: 0,
        tickThickness: 0,
        lineThickness: 0,
        labelFormatter: () => '',
        // числа 4, 24 служат для отступов, чтобы график всегда располагался в видимой области
        minimum: minY - 4,
        maximum: maxY + 24,
      },
      data: [
        {
          type: 'spline',
          markerSize: 0,
          markerColor: '#ffffff',
          color: '#ffc355',
          indexLabelFormatter: (e) => `${e.dataPoint.y}°`,
          indexLabelFontColor: '#ffffff',
          indexLabelFontSize: 14,
          indexLabelFontFamily: fontFamily,
          indexLabelFontWeight: 'normal',
          lineThickness: 2,
          dataPoints: data,
        },
      ],
    });

    this.chart.render();

    this.addPointsInfo();
  }
}
