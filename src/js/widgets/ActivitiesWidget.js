import getDistanceBetweenPoints from '../utils/getDistanceBetweenPoints';

export default class ActivitiesWidget {
  constructor({ containerID }) {
    this.mainContainer = document.body.querySelector(`#${containerID}`);
  }

  templateEngine({ data, baseGeoPoint }) {
    // Проходимся по каждому элементу данных и создаем HTML-разметку
    data.forEach((activity) => {
      const activityElement = document.createElement('div');
      activityElement.classList.add('activity');

      // Создаем ссылку для изображения
      const anchorTag = document.createElement('a');
      anchorTag.href = `${activity.link}?z=21`;
      anchorTag.setAttribute('target', '_blank');

      // Создаем изображение
      const imageElement = document.createElement('img');
      imageElement.setAttribute('class', 'activity-image');
      imageElement.src = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=180&photoreference=${activity.photoReference}&key=${import.meta.env.VITE_GOOGLE_API_KEY}`;
      imageElement.alt = activity.name;
      imageElement.title = activity.name;

      // Создаем элемент для отображения расстояния
      const distanceElement = document.createElement('div');
      distanceElement.setAttribute('class', 'activity-distance');
      distanceElement.textContent = getDistanceBetweenPoints({
        firstPoint: baseGeoPoint,
        secondPoint: activity.coords,
      });

      // Вставляем изображение внутрь ссылки
      anchorTag.appendChild(imageElement);

      // Вставляем ссылку и расстояние в элемент активности
      activityElement.appendChild(anchorTag);
      activityElement.appendChild(distanceElement);

      // Добавляем элемент активности в контейнер
      this.mainContainer.appendChild(activityElement);
    });
  }

  render({ data, baseGeoPoint }) {
    this.mainContainer.innerHTML = '';

    if (data.length) {
      this.templateEngine({ data, baseGeoPoint });
    } else {
      this.mainContainer.innerHTML = 'There are no activities in your area :(';
    }
  }
}
