import getWeatherIconByCode from '../utils/getWeatherIconByCode';
import collectCityData from '../utils/APIs/collectCityData';
import updateApp from '../utils/updateApp';

export default class MainInfoWidget {
  constructor({ containerID }) {
    this.mainContainer = document.body.querySelector(`#${containerID}`);

    // Элементы для ввода города
    this.form = this.mainContainer.querySelector('form');
    this.formInput = this.mainContainer.querySelector('form > input');
    this.notification = this.mainContainer.querySelector('.notification');

    // Информация о погоде
    this.currentWeatherName = this.mainContainer.querySelector(
      '.current-weather-name',
    );
    this.currentWeatherTemperature = this.mainContainer.querySelector(
      '.current-weather-temperature',
    );
    this.currentDate = this.mainContainer.querySelector('.current-date');
    this.currentWeatherImage = this.mainContainer.querySelector(
      '.current-weather-image',
    );
  }

  initFormHandler({
    activitiesWidget,
    mainInfoWidget,
    hourlyForecastWidget,
    airConditionsWidget,
  }) {
    // Обработчик нажатия Enter, после ввода значения в Input
    this.form.addEventListener('submit', (event) => {
      event.preventDefault();

      this.notification.classList.remove('active');

      const formInput = this.formInput.value;

      collectCityData(formInput)
        .catch((error) => {
          if (error.toString().includes('City not found!')) {
            this.notification.innerText = 'City not found! Please, try again.';
            this.notification.classList.add('warning');
            this.notification.classList.remove('info');
          } else {
            this.notification.innerText =
              'Service unavailable now. Please, try again later.';
            this.notification.classList.add('info');
            this.notification.classList.remove('warning');
          }

          this.notification.classList.add('active');
        })
        .then((cityData) => {
          updateApp({
            activitiesWidget,
            mainInfoWidget,
            hourlyForecastWidget,
            airConditionsWidget,
            cityData,
          });
        });
    });
  }

  initInputHandler() {
    this.formInput.addEventListener('keydown', (event) => {
      if (event.key === 'Backspace') {
        this.notification.classList.remove('active');
      }
    });
  }

  render({ cityName, data }) {
    this.notification.classList.remove('active');

    this.formInput.focus();

    if (!data) {
      this.notification.innerText =
        'Requested data unavailable. Please, contact support.';
      this.notification.classList.add('info');
      this.notification.classList.add('active');

      this.formInput.value = '';
      this.currentWeatherName.innerText = '';
      this.currentWeatherTemperature.innerText = '';
      this.currentDate.innerText = '';

      return;
    }

    const { weatherName, temp, dateLabel, weatherCode } = data;

    this.formInput.value = cityName;
    this.currentWeatherName.innerText = weatherName;
    this.currentWeatherTemperature.innerText = `${temp}°C`;
    this.currentDate.innerText = dateLabel;

    getWeatherIconByCode(weatherCode).then((image) => {
      this.currentWeatherImage.setAttribute('src', image);
    });
  }
}
