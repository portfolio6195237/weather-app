const cityData = { name: 'Bishkek', lat: 42.87, lon: 74.59 };

const activitiesData = [
  {
    name: 'Halil Ibrahim',
    photoReference:
      'ATplDJZt_u6G2xUoxbny9FD83S-nvfMu_Foxn4dWD10QWjlLpRljcssBKGGDEMYuDwR52wYqBN94M6zfn6qoienLAjiqY9r1uwyi8NfWcnko4gRLXQNuVZKjdfBfMUa0xM5mBnyrumRmCMpFhsq0hlZRXfb_msuLDYLr82ccU_LH59rB-cKB',
    link: 'https://maps.google.com/maps/contrib/112492293005079174194',
    coords: {
      lat: 42.8571655,
      lon: 74.5983883,
    },
  },
  {
    name: 'Empire of Pizza',
    photoReference:
      'ATplDJZeg5m-OR07sm3YFBsQpCPaB5AWISilSO8G1PRuTCUIwgUEUmSIIbb4xE4I4y1IU1jQ2PpLYFNYhJ0Ngu5tEz1vsA-peGeTv2gXPHbGTJuCo6jADpxVtwFdsW6z_sSokAi29BpEdrGTsAKIrjQwFtZl4S0ZJS9bFqtRFgQDOMm_pjLW',
    link: 'https://maps.google.com/maps/contrib/117699627156166196992',
    coords: {
      lat: 42.8749091,
      lon: 74.5827162,
    },
  },
  {
    name: 'Imperiya Pizzy',
    photoReference:
      'ATplDJYXjL1Mr6fzAGn1Smzg8RdnOX0SPOgGm5zWvJImekqjnyqO4BPLjuSOKpV9NxWAePEknCV7MYUaO82zn413xg-jbR9HxZ8g9OBlg6va0ox0uQd_KVqnea2DZJjh6ugILFkBokeQAgun_T-VG27WK5UdI-cPd6MDp2uZg9xc3PnduslS',
    link: 'https://maps.google.com/maps/contrib/104141837718758488220',
    coords: {
      lat: 42.875815,
      lon: 74.6049401,
    },
  },
  {
    name: 'Кофейня135 - Coffee135',
    photoReference:
      'ATplDJZ6_ECcebEv2rPPng9A6nGl1AjOv1QmT9us1jinhCleB3u3yu1KiRJnwZCnEEqWh46TdDofzbAh0gxdmDPHIV4e3H2hFwoqlfCUoCx9fBlTMcZIwZXVxrUzp5NXtxHckyVwcdJFAlRpFBZEmYTVI2hDzQM7pwyHDQBhj4ykwGWd7LH-',
    link: 'https://maps.google.com/maps/contrib/100696140581403380471',
    coords: {
      lat: 42.8702778,
      lon: 74.59527779999999,
    },
  },
];

const currentForecastData = {
  dateLabel: 'Tuesday | 5 Mar 2024',
  weatherName: 'Clouds',
  temp: -3,
  weatherCode: '03d',
};

const hourlyForecastData = [
  {
    x: 0,
    y: -2.85,
  },
  {
    markerSize: 5,
    x: 90,
    y: -3,
    wind: '3.3',
    time: 'Now',
    weatherCode: '03d',
  },
  {
    x: 180,
    y: -3,
    wind: '3.3',
    time: '11:00',
    weatherCode: '03d',
  },
  {
    x: 270,
    y: -2,
    wind: '3.1',
    time: '12:00',
    weatherCode: '03d',
  },
  {
    x: 360,
    y: -1,
    wind: '2.9',
    time: '13:00',
    weatherCode: '02d',
  },
  {
    x: 450,
    y: 0,
    wind: '2.4',
    time: '14:00',
    weatherCode: '02d',
  },
  {
    x: 540,
    y: 2,
    wind: '2.3',
    time: '15:00',
    weatherCode: '02d',
  },
  {
    x: 630,
    y: 3,
    wind: '2.7',
    time: '16:00',
    weatherCode: '01d',
  },
  {
    x: 720,
    y: 3,
    wind: '2.4',
    time: '17:00',
    weatherCode: '01d',
  },
];

const dailyForecastData = [
  {
    timestamp: 1709362800,
    GMTLabel: '7:00AM GMT',
    day: 'SAT',
    weatherCode: '04d',
    feelsLikeTemp: 272,
    wind: '2.2',
    chanceOfRain: null,
    UVI: 2,
  },
  {
    timestamp: 1709449200,
    GMTLabel: '7:00AM GMT',
    day: 'SUN',
    weatherCode: '04d',
    feelsLikeTemp: 269,
    wind: '4.9',
    chanceOfRain: null,
    UVI: 2,
  },
  {
    timestamp: 1709535600,
    GMTLabel: '7:00AM GMT',
    day: 'MON',
    weatherCode: '03d',
    feelsLikeTemp: 272,
    wind: '2.8',
    chanceOfRain: null,
    UVI: 3,
  },
  {
    timestamp: 1709622000,
    GMTLabel: '7:00AM GMT',
    day: 'TUE',
    weatherCode: '01d',
    feelsLikeTemp: -1,
    wind: '3.8',
    chanceOfRain: 0,
    UVI: 3,
  },
  {
    timestamp: 1709708400,
    GMTLabel: '7:00AM GMT',
    day: 'WED',
    weatherCode: '04d',
    feelsLikeTemp: 3,
    wind: '3.8',
    chanceOfRain: 0,
    UVI: 3,
  },
  {
    timestamp: 1709794800,
    GMTLabel: '7:00AM GMT',
    day: 'THU',
    weatherCode: '04d',
    feelsLikeTemp: 5,
    wind: '4.2',
    chanceOfRain: 4,
    UVI: 2,
  },
  {
    timestamp: 1709881200,
    GMTLabel: '7:00AM GMT',
    day: 'FRI',
    weatherCode: '04d',
    feelsLikeTemp: 6,
    wind: '4.8',
    chanceOfRain: 0,
    UVI: 3,
  },
];

export {
  cityData,
  activitiesData,
  currentForecastData,
  hourlyForecastData,
  dailyForecastData,
};
